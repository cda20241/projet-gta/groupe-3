package org.exo.enums;

public enum Taille {
    S,
    M,
    L,
    XL,
    XXL,
    XXXL;
}
