package org.exo.models;

public class Voiture extends Vehicule {

    // Santé max de la voiture
    private static final int MAX_PV = 100;
    // Santé carrosserie
    private static final int CARROSSERIE_PV = 100;
    // Dégats des collisions
    public static final int NB_POINT_COLL = 10;

    public Voiture() {
        super(MAX_PV, CARROSSERIE_PV);
    }


    //___METHODES___//

    /**
     * Cette méthode set les points de vie du moteur du véhicule à 0.
     */
    public void sousleau() {
        setMoteurPointsDeVie(super.LOW_PV);
    }
}
