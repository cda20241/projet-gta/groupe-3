package org.exo.models;

import org.exo.enums.Coupe;
import org.exo.interfaces.IMetier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Coiffeur extends Personnage implements IMetier {

    // Constants

    private final static int TARIF_COUPE = 15;
    private final static int TARIF_EXTENSIONS = 20;
    private final static int CHOIX_COUPE = 1;
    private final static int CHOIX_EXTENSIONS = 2;
    private final static int EXIT = 0;
    private final static int LONGUEUR_MAX_EXT = 40;
    private final static int LONGUEUR_MAX_TOTALE = 60;
    private final static int LONGUEUR_MIN = 0;
    private final static ArrayList<String> COMPETENCES_COIFFEUR =
            new ArrayList<>(Arrays.asList("Faire une coupe", "Poser des extensions"));

    // Constructors

    public Coiffeur() {
        super();
    }

    public Coiffeur(String pseudo, String sexe, Integer taille, Integer longueurCheveux){
        super(pseudo, sexe, taille, longueurCheveux);
    }

    // Methods

    /**
     * Cette méthode liste les compétences d'un coiffeur definies dans la constante de type ArrayList "COMPETENCES_COIFFEUR".
     * Elle renvoie le choix (entier) qu'aura saisi l'utilisateur.
     * @return choix : int
     */
    @Override
    public int listerCompetences() {
        int index = 1;
        int choix = -1;
        for (String item : COMPETENCES_COIFFEUR){
            System.out.println(COMPETENCES_COIFFEUR.get(index - 1) + " : entrez " + index);
            index++;
        }
        System.out.println("Votre choix : ");
        Scanner scanner = new Scanner(System.in);
        choix = Integer.parseInt(scanner.nextLine());
        return choix;
    }

    /**
     * Cette méthode appelle une autre méthode en fonction du choix saisit par l'utilisateur.
     */
    @Override
    public void effectuerTravail(int choix, Vehicule vehicule, Personnage personnage) {
        Scanner scanner = new Scanner(System.in);
        int longueur = -1;
        // Si le choix de l'utilisateur est "CHOIX_COUPE", on liste les coupes disponibles dans l'enumération "Coupe"
        if (choix == CHOIX_COUPE){
            int index = 1;
            for (Coupe coupe :  Coupe.values()){
                System.out.println(coupe.getLabel() + " : " + coupe.getValue() + " cm, entrez " + coupe.getValue());
                index ++;
            }
        }
        // Dans tous les cas, on demande la longueur (de coupe ou de rallonge).
        System.out.println("Quelle longueur voulez-vous ?");
        longueur = Integer.parseInt(scanner.nextLine());
                switch(choix){
            case CHOIX_COUPE:
                faireUneCoupe(personnage, longueur);
                break;
            case CHOIX_EXTENSIONS:
                poserExtensions(personnage, longueur);
                break;
            case EXIT:
                System.exit(EXIT);
        }
    }

    /**
     * Cette méthode débite un personnage selon un tarif passé en paramètre.
     * @param personnage {@link Personnage}
     * @param tarif int
     */
    @Override
    public void facturerClient(Personnage personnage, int tarif) {
        System.out.println("\nVotre ancien solde : " + personnage.getArgent() + " crédits.");
        System.out.println("Vous avez été débité de " + tarif + " crédits.");
        personnage.setArgent(personnage.getArgent() - tarif);
        System.out.println("Votre nouveau solde : " + personnage.getArgent() + " crédits.");
    }

    /**
     * Cette méthode réduit la taille des cheveux du personne selon une longueur passée en paramètre et définie par l'utilisateur.
     * @param personnage {@link Personnage}
     * @param longueur int
     */
    public void faireUneCoupe(Personnage personnage, int longueur){
        // Si le personnage ne possède pas assez d'argent pour payer la coupe
        if (personnage.getArgent() < TARIF_COUPE){
            System.out.println("\nVous n'avez pas assez d'argent pour vous offrir une coupe !");
        }
        else{
            // Si la longueur des cheuveux du personnage est inférieure ou égale à la longueur demandée, on redemande une autre longueur.
            if (personnage.getLongueurCheveux() <= longueur){
                System.out.println("\nCoupe impossible, cheuveux trop courts.");
                effectuerTravail(CHOIX_COUPE, null, personnage);
            }
            // Si la longueur des cheuveux est = 0 ==> coupe impossible.
            else if (personnage.getLongueurCheveux() == LONGUEUR_MIN) {
                System.out.println("\nCoupe impossible, pas de cheuveux.");
            } else{
                System.out.println("\nVos cheuveux ont été raccourcis de : " + (personnage.getLongueurCheveux() - longueur) + " cm.");
                personnage.setLongueurCheveux(longueur);
                System.out.println("Ils mesurent maintenant : " + personnage.getLongueurCheveux() + " cm.");
                facturerClient(personnage, TARIF_COUPE);
            }
        }

    }

    /**
     * Cette méthode allonge la taille des cheuveux d'un personnage selon une longueur passée en paramètre et saisie par l'utilisateur.
     * @param personnage {@link Personnage}
     * @param longueur int
     */
    public void poserExtensions(Personnage personnage, int longueur){
        // Si le personnage ne possède pas assez d'argent pour payer la coupe
        if (personnage.getArgent() < TARIF_COUPE){
            System.out.println("\nVous n'avez pas assez d'argent pour vous offrir des extensions !");
        }
        else{
            // Tests sur la longueur des cheuveux :
            // La longueur des extensions doit être <= 40cm
            if (longueur > LONGUEUR_MAX_EXT){
                System.out.print("\nPose extensions impossible, longueur demandée > 40 cm.");
            }
            // La longueur initiale des cheuveux du perso doit être > 0
            else if (personnage.getLongueurCheveux() == LONGUEUR_MIN){
                System.out.print("\nPose extensions impossible, pas de cheuveux.");
            }
            // La longueur totale (cheuveux perso + extensions) doit être <= 60cm
            else if (personnage.getLongueurCheveux() + longueur > LONGUEUR_MAX_TOTALE){
                System.out.print("\nPose extensions impossible, longueur totale demandée > 60 cm.");
            }
            else {
                System.out.print("\nVos cheuveux ont été rallongés de : " + longueur + " cm.");
                personnage.setLongueurCheveux(personnage.getLongueurCheveux() + longueur);
                System.out.print("Ils mesurent maintenant : " + personnage.getLongueurCheveux() + " cm.");
                facturerClient(personnage, TARIF_EXTENSIONS);
            }
        }
    }
}
