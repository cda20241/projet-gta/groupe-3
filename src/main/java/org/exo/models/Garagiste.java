package org.exo.models;

import org.exo.interfaces.IMetier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Garagiste extends Personnage implements IMetier {

    // Constants

    private final static int TARIF_CARROSSERIE = 15;
    private final static int TARIF_MOTEUR = 25;
    private final static int TARIF_PEINTURE = 10;
    private final static int CHOIX_CARROSSERIE = 1;
    private final static int CHOIX_MOTEUR = 2;
    private final static int CHOIX_PEINTURE = 3;
    private final static int POINT_VIE_MAX = 100;
    private final static int EXIT = 0;
    private final static ArrayList<String> COMPETENCES_GARAGISTE =
            new ArrayList<>(Arrays.asList("Réparer la carrosserie", "Réparer le moteur", "Refaire la peinture"));

    // Constructors

    public Garagiste() {
        super();
    }

    public Garagiste(String pseudo, String sexe, Integer taille, Integer longueurCheveux){
        super(pseudo, sexe, taille, longueurCheveux);
    }

    // Methods

    /**
     * Cette méthode liste les compétences d'un garagiste definies dans la constante de type ArrayList "COMPETENCES_GARAGISTE".
     * Elle renvoie le choix (entier) qu'aura saisi l'utilisateur.
     * @return choix : int
     */
    @Override
    public int listerCompetences() {
        int index = 1;
        int choix = -1;
        for (String item : COMPETENCES_GARAGISTE){
            System.out.println(item + " : entrez " + index);
            index++;
        }
        System.out.println("Sortir : entrez " + EXIT);
        System.out.println("Votre choix : ");
        Scanner scanner = new Scanner(System.in);
        choix = Integer.parseInt(scanner.nextLine());
        return choix;
    }

    /**
     * Cette méthode appelle une autre méthode en fonction du choix saisit par l'utilisateur.
     */
    @Override
    public void effectuerTravail(int choix, Vehicule vehicule, Personnage personnage) {
        switch(choix){
            case CHOIX_CARROSSERIE:
                reparerCarrosserie(vehicule, personnage);
                break;
            case CHOIX_MOTEUR:
                reparerMoteur(vehicule, personnage);
                break;
            case CHOIX_PEINTURE:
                repeindreVehicule(vehicule, personnage);
                break;
            case EXIT:
                System.exit(EXIT);
        }
    }

    /**
     * Cette méthode débite un personnage selon un tarif passé en paramètre.
     * @param personnage {@link Personnage}
     * @param tarif int
     */
    @Override
    public void facturerClient(Personnage personnage, int tarif) {
        System.out.println("\nVotre ancien solde : " + personnage.getArgent() + " crédits.");
        System.out.println("Vous avez été débité de " + tarif + " crédits.");
        personnage.setArgent(personnage.getArgent() - tarif);
        System.out.println("Votre nouveau solde : " + personnage.getArgent() + " crédits.");
    }

    /**
     * Cette méthode reset au max les points de vie de la carrosserie d'un véhicule et facture le personnage.
     * @param vehicule {@link Vehicule}
     * @param personnage {@link Personnage}
     */
    public void reparerCarrosserie(Vehicule vehicule, Personnage personnage){
        if (personnage.getArgent() < TARIF_CARROSSERIE){
            System.out.println("\nVotre solde ne permet pas de faire réparer la carrosserie.");
        }
        else{
            vehicule.setCarrosseriePointsDeVie(POINT_VIE_MAX);
            System.out.println("\nVotre carrosserie a été réparée.");
            facturerClient(personnage, TARIF_CARROSSERIE);
        }
    }

    /**
     * Cette méthode reset au max les points de vie du moteur d'un véhicule et facture le personnage.
     * @param vehicule {@link Vehicule}
     * @param personnage {@link Personnage}
     */
    public void reparerMoteur(Vehicule vehicule, Personnage personnage){
        if (personnage.getArgent() < TARIF_MOTEUR){
            System.out.println("\nVotre solde ne permet pas de faire réparer le moteur.");
        }
        else{
            vehicule.setMoteurPointsDeVie(POINT_VIE_MAX);
            System.out.println("\nVotre moteur a été réparé.");
            facturerClient(personnage, TARIF_MOTEUR);
        }
    }

    /**
     * Cette méthode change la couleur de la carrosserie d'un véhicule et facture le personnage.
     * @param vehicule {@link Vehicule}
     * @param personnage {@link Personnage}
     */
    public void repeindreVehicule(Vehicule vehicule, Personnage personnage){
        if (personnage.getArgent() < TARIF_PEINTURE){
            System.out.println("\nVotre solde ne permet pas de faire réparer le moteur.");
        }
        else{
            Scanner scanner = new Scanner(System.in);
            String couleur = "";
            System.out.println("\nQuelle couleur voulez-vous ?");
            couleur = scanner.nextLine();
            vehicule.setCouleur(couleur);
            System.out.println("Votre voiture a été repeinte en " + couleur + ".");
            facturerClient(personnage, TARIF_PEINTURE);
        }
    }
}
