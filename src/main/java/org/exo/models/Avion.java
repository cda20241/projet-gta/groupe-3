package org.exo.models;

public class Avion extends Vehicule{

    // Santé max de la voiture
    private static final int MAX_PV = 100;
    // Santé carrosserie
    private static final int CARROSSERIE_PV = 100;
    // Dégats des collisions
    public static final int NB_POINT_COLL = 100;

    public Avion() {
        super(MAX_PV, CARROSSERIE_PV);
    }


}
