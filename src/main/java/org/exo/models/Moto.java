package org.exo.models;

public class Moto extends Vehicule {

    // Santé max de la voiture
    private static final int MAX_PV = 50;
    // Santé carrosserie
    private static final int CARROSSERIE_PV = 100;
    // Dégats des collisions
    public static final int NB_POINT_COLL = 50;

    public Moto() {
        super(MAX_PV, CARROSSERIE_PV);
    }


}
