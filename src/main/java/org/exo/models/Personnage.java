package org.exo.models;

import java.util.ArrayList;
import java.util.List;

//On créer un personnage



public class Personnage {
    private String pseudo;
    private String sexe;
    private Integer taille;
    private Integer longueurCheveux;
    private Integer argent;
    private List<Vetement> vetements;
    private List<Vehicule> vehicules;
    private static final Integer MAX_ARGENT = 100;

    //___CONSTRUCTOR___//

    public Personnage(String pseudo, String sexe, Integer taille, Integer longueurCheveux) {
        this.pseudo = pseudo;
        this.sexe = sexe;
        this.taille = taille;
        this.argent = MAX_ARGENT;
        this.longueurCheveux = longueurCheveux;
        this.vetements = new ArrayList<>();
        this.vehicules = new ArrayList<>();
    }

    public Personnage() {

    }

    //___GETTER_SETTER___//

    public String getPseudo() {
        return pseudo;
    }

    public String getSexe() {
        return sexe;
    }

    public Integer getTaille() {
        return taille;
    }

    public Integer getArgent() {
        return argent;
    }

    public void setArgent(Integer argent) {
        this.argent = argent;
    }

    public Integer getLongueurCheveux() {return longueurCheveux;}

    public void setLongueurCheveux(Integer longueurCheveux) {this.longueurCheveux = longueurCheveux;}

    public List<Vetement> getVetements() {return vetements;}

    public List<Vehicule> getVehicules() {return vehicules;}

    public void setVehicules(Vehicule vehicule) {vehicules.add(vehicule);}


    //___METHODS___//

    /**
     * Cette méthode ajoute un objet detype Vetement à la liste de vêtements du personnage.
     * @param vetement : Vetement
     */
    public void addVetement(Vetement vetement) {vetements.add(vetement);}

    /**
     * Cette méthode permet d'avoir la taille totale du personnage lorsqu'il porte des chaussures.
     */
    public void setTailleTotale() {
        /*
        Récupère les vêtements portés par la personne et se débarasse de tous les strings contenus dans tailleVetement,
        une fois qu'il ne reste que des integers, ajoute ces integers à la taille de la personne.
        */

        int taillechaussures = vetements
        // 100% du stackoverflow, je confesse.
                .stream() // Transforme la liste vetements en stream
                .map(Vetement::getTaille) // Créer un dictionnaire (map) en assignant la valeur getTaille à l'objet Vetement
                .filter(Integer.class::isInstance) // Filtre les valeurs pour ne garder que les Integer
                .mapToInt(Integer.class::cast) // Converti chaque element de la map en Integer
                .sum(); // Récupère la somme des Integer
        taille = taille + taillechaussures;
    }



}
