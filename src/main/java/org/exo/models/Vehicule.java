package org.exo.models;

public class Vehicule {
    private String marque;
    private String modele;
    private String couleur;
    private Moteur moteur;
    private Integer carrosseriePointsDeVie;
    private Personnage proprietaire;
    private Personnage conducteur;
    public static final Integer PRIX_VEHICULE = 30;
    public static final int LOW_PV = 0;


    public Vehicule(){

    }
    public Vehicule(Integer moteurPointsDeVie, Integer carrosseriePointsDeVie) {
        this.marque = marque;
        this.modele = modele;
        this.couleur = couleur;
        this.moteur = new Moteur(moteurPointsDeVie);
        this.carrosseriePointsDeVie = carrosseriePointsDeVie;
    }


    //_____ACHETER VEHICULE_____//

    /**
     * Cette méthode ajoute le véhicule à la liste de véhicules d'un personnage et en fait le propriétaire.
     * @param personnage : Personnage
     */
    public void acheterVehicule(Personnage personnage) {
        /*
        Si la personne dispose d'assez d'argent, elle pourra acheter un véhicule.
         */
        if(personnage.getArgent() >= Vehicule.PRIX_VEHICULE ){
            personnage.getVehicules().add(this);
            this.setProprietaire(personnage);
            personnage.setArgent(personnage.getArgent() - Vehicule.PRIX_VEHICULE);
            System.out.println("\nVous venez d'acquérir un véhicule.");
            System.out.println("\nVotre ancien solde : " + personnage.getArgent() + " crédits.");
            System.out.println("Vous avez été débité de " + PRIX_VEHICULE + " crédits.");
            System.out.println("Votre nouveau solde : " + (personnage.getArgent() - PRIX_VEHICULE) + " crédits.");
            personnage.setArgent(personnage.getArgent() - PRIX_VEHICULE);
        } else System.out.println("Vous n'avez pas assez d'argent pour acheter un véhicule.");



    }

    //_____CONDUIRE VEHICULE_____//

    /**
     * Cette méthode permet d'ajouter un conducteur au véhicule.
     * @param conducteur : Personnage
     */
    public void conduireVehicule(Personnage conducteur) {
        // Si les points de dégâts de la voiture tombent à zéro, plus personne ne peut être conducteur de la voiture ni propriétaire.
        // La voiture disparaît du jeu.
        if (this.getConducteur() == null) {
            this.setConducteur(conducteur);
        }
        else if (this.getConducteur() != conducteur){
            System.out.println("\nLe véhicule ne peut pas être conduit par plusieurs conducteurs en même temps.");
        }
        if (this.getCarrosseriePointsDeVie() <= LOW_PV) {
            this.setConducteur(null);
            this.setProprietaire(null);
            conducteur.getVehicules().remove(this);
        } else if (this.getMoteurPointsDeVie() <= LOW_PV){
            System.out.println("\nLe véhicule ne peut plus démarrer.");
        } else {
            System.out.println("\nLe véhicule démarre.");
        }
    }

    //___SORTIR_DU_VEHICULE___//

    /**
     * Cette méthode set le conducteur du véhicule à null.
     * @param personnage : Personnage
     */

    public void sortirDuVehicule(Personnage personnage){
        this.setConducteur(null);
        System.out.println("Vous êtes sorti du véhicule.");
    }

    //___COLLISION___//

    /**
     * Cette méthode soustrait des points de vie au moteur et carosserie du véhicule.
     * @param pointColllision int
     */
    public void collision(int pointColllision) {
        // Réduire les points de vie du moteur et de la carrosserie en cas de collision
        this.setMoteurPointsDeVie(this.getMoteurPointsDeVie() - pointColllision);
        this.setCarrosseriePointsDeVie(this.getCarrosseriePointsDeVie() - pointColllision);
    }


    //_____GETTER_____//
    public Integer getMoteurPointsDeVie() {return moteur.getPointsDeVie();}
    public String getMarque() {return marque;}
    public String getModele() {return modele;}
    public String getCouleur() {return couleur;}
    public Moteur getMoteur() {return moteur;}
    public Integer getCarrosseriePointsDeVie() {return carrosseriePointsDeVie;}
    public Personnage getProprietaire() {return proprietaire;}
    public Personnage getConducteur() {return conducteur;}

    //_____SETTER_____//
    public void setMoteurPointsDeVie(Integer pointsDeVie) {moteur.setPointsDeVie(pointsDeVie);}
    public void setMarque(String marque) {this.marque = marque;}
    public void setModele(String modele) {this.modele = modele;}
    public void setCouleur(String couleur) {this.couleur = couleur;}
    public void setMoteur(Moteur moteur) {this.moteur = moteur;}
    public void setCarrosseriePointsDeVie(Integer carrosseriePointsDeVie) {this.carrosseriePointsDeVie = carrosseriePointsDeVie;}
    public void setProprietaire(Personnage proprietaire) {this.proprietaire = proprietaire;}
    public void setConducteur(Personnage conducteur) {this.conducteur = conducteur;}

}


