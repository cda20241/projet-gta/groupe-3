package org.exo.utils;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.exo.models.Personnage;
import org.exo.models.Vehicule;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Util {

    private static final String PERSO_JSON_PATH = "src/main/resources/Perso.json";
    private static final String VEHICULE_JSON_PATH = "src/main/resources/Vehicule.json";

    /**
     * Cette fonction supprime les fichiers Json de sauvegarde
     */
    public static void deleteJsonFiles(){
        File fileTmp1 = new File(PERSO_JSON_PATH);
        if (fileTmp1.exists()){
            fileTmp1.delete();
        }
        File fileTmp2 = new File(VEHICULE_JSON_PATH);
        if (fileTmp2.exists()){
            fileTmp2.delete();
        }
    }

    /**
     * Cette fonction convertit un objet de type Personnage en une String au format Json et l'enregistre dans le fichier spécifié.
     * @param personnage Personnage
     */
    public static void serializePersonToFile (Personnage personnage) {
        // Create a Gson instance
        // Define the file path where you want to save the JSON
        FileWriter writer = null;
        Gson gson = new GsonBuilder()
            .setExclusionStrategies(new ExclusionStrategy() {
                @Override
                public boolean shouldSkipField(FieldAttributes f) {
                    // Exclude fields that cause circular references
                    return f.getDeclaringClass() == Vehicule.class &&
                            (f.getName().equals("proprietaire") || f.getName().equals("conducteur"));
                }

                @Override
                public boolean shouldSkipClass(Class<?> clazz) {
                    return false;
                }
            })
            .create();
        try {
            // Serialize the object to JSON
            String json = gson.toJson(personnage);

            // Write the JSON to a file
            try  {
                writer = new FileWriter(PERSO_JSON_PATH, true);
                writer.write(json);
                writer.write("\n");
            }
            finally{
                writer.close();
            }
            System.out.println("Object serialized and written to " + PERSO_JSON_PATH);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Cette fonction convertit un objet de type Vehicule en une String au format Json
     * et l'enregistre dans le fichier spécifié.
     * @param vehicule Vehicule
     */
    public static void serializeVehiculeToFile(Vehicule vehicule) {
        // Define the file path where you want to save the JSON
        // Create a Gson instance
        FileWriter writer = null;
        Gson gson = new GsonBuilder()
        .setExclusionStrategies(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                // Exclude fields that cause circular references
                return f.getDeclaringClass() == Personnage.class &&
                        (f.getName().equals("vetements") || f.getName().equals("vehicules"));
            }

            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        })
        .create();
        try {
            // Serialize the object to JSON
            String json = gson.toJson(vehicule);
            // Write the JSON to a file
            try  {
                writer = new FileWriter(VEHICULE_JSON_PATH, true);
                writer.write(json);
                writer.write("\n");
            }
            finally{
                writer.close();
            }
            System.out.println("Object serialized and written to " + VEHICULE_JSON_PATH);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Cette fonction lit un fichier Json et convertit chaque ligne en un objet de type Personnage
     * et créé une liste de personnages avant de la renvoyer.
     * @return List<Personnage>
     */
    public static List<Personnage> readJsonFileAndRenderPerso() {
        // Create a Gson instance
        Gson gson = new GsonBuilder().create();

        // Initialize a list to store the deserialized Personnage objects
        List<Personnage> personnages = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(PERSO_JSON_PATH))) {
            String line;
            StringBuilder jsonContent = new StringBuilder();

            while ((line = reader.readLine()) != null) {
                // Append each line from the file to the StringBuilder
                jsonContent.append(line);
                // Check if the line is the end of a JSON object (assuming each JSON object is on a single line)
                if (line.substring(line.trim().length() - 1).equals("}")) {
                    // Deserialize the JSON object and add it to the list
                    Personnage personnage = gson.fromJson(jsonContent.toString(), Personnage.class);
                    personnages.add(personnage);
                    // Clear the StringBuilder for the next JSON object
                    jsonContent.setLength(0);
                }
            }
        } catch (IOException e) {
            System.out.println("WARNING : Le fichier de sauvegarde \"Perso.json\" n'éxiste pas.");
        }
        return personnages;
    }

    /**
     * Cette fonction lit un fichier Json et convertit chaque ligne en un objet de type Vehicule
     * et créé une liste de vehicules avant de la renvoyer.
     * @return List<Vehicule>
     */
    public static List<Vehicule> readJsonFileAndRenderVehicule() {
        // Create a Gson instance
        Gson gson = new GsonBuilder().create();
        List<Vehicule> vehicules = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(VEHICULE_JSON_PATH))) {
            String line;
            StringBuilder jsonContent = new StringBuilder();

            while ((line = reader.readLine()) != null) {
                // Append each line from the file to the StringBuilder
                jsonContent.append(line);
                // Check if the line is the end of a JSON object (assuming each JSON object is on a single line)
                if (line.substring(line.trim().length() - 1).equals("}")) {
                    // Deserialize the JSON object and add it to the list
                    Vehicule vehicule = gson.fromJson(jsonContent.toString(), Vehicule.class);
                    vehicules.add(vehicule);
                    // Clear the StringBuilder for the next JSON object
                    jsonContent.setLength(0);
                }
            }
        } catch (IOException e) {
            System.out.println("WARNING : Le fichier de sauvegarde \"Vehicule.json\" n'éxiste pas.");
        }
        return vehicules;
    }
}



